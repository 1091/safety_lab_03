#!/usr/bin/env python

import sys
import string
import datetime
import functools

from Crypto.Hash import SHA256
from Crypto.Random import random


def get_clipped_sha256(sha256, message, start_bit, size):
    """Returns clipped sha256 digest by given message,
    start bit and size of clip, return value is string
    of binary values e.g. '01010100100110'.

    """
    if start_bit + size > 256 or start_bit < 0 or size < 0:
        raise Exception("Wrong size and start bit")
    sha256.update(message)
    digest = sha256.digest()
    digest_bin = []
    for character in digest:
        binary = bin(ord(character))[2:].zfill(8)
        digest_bin.append(binary)
    digest_bin = ''.join(digest_bin)
    digest_bin = digest_bin[start_bit:start_bit + size]
    return digest_bin


def get_digest_slice(hash_string, start_bit, size):
    """
    Returns slice of given hexadecimal digest string, which
    starts in start_bit and have given size.

    """
    if(len(hash_string) != 32*2):
        raise Exception("hash_slice: wrong hash length")
    if(start_bit < 0 or size < 0 or start_bit + size > 32 * 2):
        raise Exception("hash_slice: wrong start_bit/size params")

    hash_list_string = (
        [hash_string[i:i+2] for i in range(0, len(hash_string), 2)]
    )
    hashList = list(map(lambda elem: int(elem, 16), hash_list_string))
    start_byteShift = start_bit / 8
    start_bitShift = start_bit % 8

    last_bitShift = (start_bit + size) % 8
    last_byteShift = (start_bit + size) / 8 + (1 if last_bitShift > 0 else 0)
    resList = []
    if(start_byteShift == last_byteShift):
        return '{:02x}'.format(
            (hashList[last_byteShift] & ~(255 >> last_bitShift))
        )
    first_byte = hashList[start_byteShift] & (255 >> start_bitShift)
    last_byte = hashList[last_byteShift] & ~(255 >> last_bitShift)

    resList.append(first_byte)
    for i in range(start_byteShift+1, last_byteShift-1):
        resList.append(hashList[i])
    resList.append(last_byte)
    resStr = ""
    for x in range(0, len(resList)):
        resStr += '{:02x}'.format(resList[x])
    return resStr


def get_random_string():
    random_string = ""
    size = random.randint(16, 64)
    for i in range(size):
        random_string += chr(random.randint(ord(' '), ord('~')))
    return random_string


def string_generator():
    chars = string.ascii_letters + string.digits
    length = random.randint(16, 64)
    return ''.join(random.choice(chars) for _ in range(length))


def find_collision(sha256, start_bit, size):
    start_time = datetime.datetime.now()
    counter = 0
    found = False
    messages = []
    while not found:
        message = get_random_string()
        sha256.update(message)
        digest = sha256.hexdigest()
        digest = get_digest_slice(digest, start_bit, size)
        messages.append((message, digest))
        sys.stdout.write("\r %s %s" % (digest, message))
        sys.stdout.flush()
        for message_ in messages:
            if digest == message_[1] and message != message_[0]:
                print (
                    "kollision:\n"
                    "\n" + str(message_[0]) + " " + message_[1] +
                    "\n" + message + " " + digest
                )
                found = True
        counter += 1
    end_time = datetime.datetime.now()
    print counter
    return end_time - start_time

my_hash = SHA256.new()
# get_clipped_sha256 = functools.partial(get_clipped_sha256, my_hash)
find_collision = functools.partial(find_collision, my_hash)
print find_collision(13, 20)
